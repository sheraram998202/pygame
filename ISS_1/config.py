import pygame
import random
import math
from pygame import mixer
pygame.init()
screen = pygame.display.set_mode((1200, 793))
winner=pygame.image.load('winner.jpg')
#Plyer
pl1Img=pygame.image.load('p1.png')
colhs=0
coldk=0
colsh=0
colbt=0
pl1X=100
pl1Y=740
pl1X_change=0
pl1Y_change=0
pl2Img=pygame.image.load('p1.png')
pl2X=100
pl2Y=0
pl2X_change=0
pl2Y_change=0
landImg=[]
landX=[]
landY=[]
nl=6

for i in range(nl):
    landImg.append(pygame.image.load('land.jpg'))
    landX.append(0)
    landY.append(121*i + 61)

houseImg=[]
houseX=[]
houseY=[]
nh=6
for h in range(nh):
    houseImg.append(pygame.image.load('house.png'))
    houseX.append(random.randint(50,1150))
    houseY.append(121*h + 90)

duckImg=[]
duckX=[]
duckY=[]
nd=6
for d in range(nd):
    duckImg.append(pygame.image.load('oak.png'))
    duckX.append(random.randint(30,1170))
    duckY.append(121*d + 65)

# moveble component
boatImg=[]
boatX=[]
boatY=[]
boatX_change=[]
nb=5
for j in range(nb):
    boatImg.append(pygame.image.load('boat.png'))
    boatX.append(400*j + 200)
    boatY.append(121*j + 121)
    boatX_change.append((j*j + 2*j + 4)%8)
shipImg=[]
shipX=[]
shipY=[]
shipX_change=[]
ns=5
for s in range(ns):
    shipImg.append(pygame.image.load('ship.png'))
    shipX.append(345*s+700)
    shipY.append(121*s + 121)
    shipX_change.append((s*s)%4 + 2)

# Background
background = pygame.image.load('BC.jpg')
mixer.music.load("background.wav")
mixer.music.play(-1)

icon=pygame.image.load('bc.jpg')
pygame.display.set_icon(icon)
pygame.display.set_caption("shera")

def collhouse(houseX,houseY,pl1X,pl1Y):
    distance=math.sqrt(math.pow(houseX-pl1X , 2) + (math.pow(houseY-pl1Y,2)))
    if distance<15:
        return True
    else:
        return False

def collduck(duckX,duckY,pl1X,pl1Y):
    distance=math.sqrt(math.pow(duckX-pl1X , 2) + (math.pow(duckY-pl1Y,2)))
    if distance<15:
        return True
    else:
        return False
def collboat(boatX,boatY,pl1X,pl1Y):
    distance=math.sqrt(math.pow(boatX-pl1X , 2) + (math.pow(boatY-pl1Y,2)))
    if distance<30:
        return True
    else:
        return False
    print("kjmng")    
def collship(shipX,shipY,pl1X,pl1Y):
    distance=math.sqrt(math.pow(shipX-pl1X , 2) + (math.pow(shipY-pl1Y,2)))
    if distance<25:
        return True
    else:
        return False
def pl1(x,y):
    screen.blit(pl1Img,(x,y))
def boat(x,y,j):
    screen.blit(boatImg[j],(x,y))
def ship(x,y,s):
    screen.blit(shipImg[s],(x,y))
#collision
score_value1=0
score_value2=0
font=pygame.font.Font('freesansbold.ttf',32)


st1X=580
st1Y=763
def start1(x,y):
    st1=font.render("start",True,(123,22,34))
    screen.blit(st1,(x,y))
font=pygame.font.Font('freesansbold.ttf',32)
scX=10
scY=10
def sh1_score(x,y):
    score=font.render("Score_p1 : "+str(score_value1),True,(255,255,255))
    screen.blit(score,(x,y))
def sh2_score(x,y):
    score=font.render("Score_p2 : "+str(score_value2),True,(255,255,255))
    screen.blit(score,(x,y))

st1X=580
st1Y=763
def start1(x,y):
    st1=font.render("start",True,(123,22,34))
    screen.blit(st1,(x,y))
st2X=580
st2Y=0
def start2(x,y):
    st2=font.render("start",True,(123,22,34))
    screen.blit(st2,(x,y))
en2X=580
en2Y=763
en1X=580
en1Y=0
def end1(x,y):
    ed1=font.render("End",True,(123,23,234))
    screen.blit(ed1,(x,y))
st2X=580
st2Y=0
def start2(x,y):
    st2=font.render("start",True,(123,22,34))
    screen.blit(st2,(x,y))
en2X=580
en2Y=763
en1X=580
en1Y=0
def end1(x,y):
    ed1=font.render("End",True,(123,23,234))
    screen.blit(ed1,(x,y))
def end2(x,y):
    ed2=font.render("End",True,(123,32,234))
    screen.blit(ed2,(x,y))

#end_game
game_end=pygame.font.Font('freesansbold.ttf',64)
def end1_game():
    game_end_text=game_end.render("P1 hit obstacle",True,(255,255,255))
    screen.blit(game_end_text,(400,350))
def end2_game():
    game_end_text=game_end.render("P2 hit obstacle",True,(255,255,255))
    screen.blit(game_end_text,(400,350))
#reach end
def end2_race():
    end2_rac=game_end.render("P2 reach the goal",True,(255,255,255))
    screen.blit(end2_rac,(400,350))
def end1_race():
    end1_rac=game_end.render("P1 reach the goal",True,(255,255,255))
    screen.blit(end1_rac,(400,350))
shX=400
shY=350
def pl1_show():
    pl1_show_text=game_end.render("Pl1 won ",True,(233,234,12))
    screen.blit(pl1_show_text,(400,350))
def pl2_show():
    pl2_show_text=game_end.render("Pl2 won ",True,(233,234,12))
    screen.blit(pl2_show_text,(400,350))
def eq_show():
    eq_show_text=game_end.render("Match tie",True,(233,234,12))
    screen.blit(eq_show_text,(400,350))

